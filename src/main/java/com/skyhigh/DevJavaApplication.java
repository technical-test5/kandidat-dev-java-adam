package com.skyhigh;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class DevJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevJavaApplication.class, args);
	}

	@Autowired
	private ObjectMapper objectMapper;

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
