package com.skyhigh.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import com.skyhigh.entity.SalesInvoiceLine;

import lombok.Data;

@Data
public class SalesInvoiceDTO {

    private UUID salesInvoiceId;

    private UUID CustomerId;

    private String invNumber;

    private LocalDate invDate;

    private BigDecimal amount;

    private BigDecimal totalDiscountAmount;

    private BigDecimal totalAmount;
    private List<SalesInvoiceLine> detail;
}
