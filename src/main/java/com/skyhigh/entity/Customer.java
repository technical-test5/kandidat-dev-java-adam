package com.skyhigh.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "customer")
@Data
public class Customer {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "customer_id")
    private UUID customerId;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 100)
    @Column(name = "customer_name")
    private String customerName;

    @NotNull
    @NotEmpty
    @Size(max = 100)
    @Column(name = "customer_address")
    private String customerAddress;

    @NotNull
    @NotEmpty
    @Size(max = 100)
    @Email
    @Column(name = "customer_email")
    private String customerEmail;

    @NotNull
    @NotEmpty
    @Size(max = 30)
    @Column(name = "customer_phone")
    private String customerPhone;

    // public String getCustomerId() {
    // return customerId.toString();
    // }

    // public void setCustomerId(UUID customerId) {
    // this.customerId = customerId;
    // }

    // public String getCustomerName() {
    // return customerName;
    // }

    // public void setCustomerName(String customerName) {
    // this.customerName = customerName;
    // }

    // public String getCustomerAddress() {
    // return customerAddress;
    // }

    // public void setCustomerAddress(String customerAddress) {
    // this.customerAddress = customerAddress;
    // }

    // public String getCustomerEmail() {
    // return customerEmail;
    // }

    // public void setCustomerEmail(String customerEmail) {
    // this.customerEmail = customerEmail;
    // }

    // public String getCustomerPhone() {
    // return customerPhone;
    // }

    // public void setCustomerPhone(String customerPhone) {
    // this.customerPhone = customerPhone;
    // }

}
