package com.skyhigh.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.persistence.JoinColumn;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.skyhigh.serializable.SalesInvoiceLineSerializable;
import com.skyhigh.serializable.SalesInvoiceSerializable;

import lombok.Data;

@Entity
@Table(name = "sales_invoice_line")
@Data
@IdClass(SalesInvoiceLineSerializable.class)
public class SalesInvoiceLine implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "sales_invoice_line_id")
    private UUID salesInvoiceLineId;

    @Id
    @Column(name = "t_sales_invoice_id")
    private UUID TsalesInvoiceId;

    @Column(name = "m_product_id")
    private UUID mProductId;

    @Min(0)
    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "discount_amount")
    private BigDecimal discountAmount;

    @Column(name = "total_amount")
    private BigDecimal totalAmount;

    @ManyToOne
    @JoinColumn(name = "t_sales_invoice_id", insertable = false, updatable = false)
    @JsonIgnoreProperties({ "line" })
    private SalesInvoice sales;

    @OneToOne
    @JoinColumns(value = {
            @JoinColumn(name = "m_product_id", referencedColumnName = "product_id", insertable = false, updatable = false, nullable = false) })
    private Product product;
}
