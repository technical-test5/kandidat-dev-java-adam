package com.skyhigh.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumns;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.persistence.JoinColumn;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.skyhigh.serializable.SalesInvoiceSerializable;

import lombok.Data;

@Entity
@Table(name = "sales_invoice")
@Data
public class SalesInvoice implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "sales_invoice_id")
    private UUID salesInvoiceId;

    @Column(name = "customer_id")
    private UUID CustomerId;

    @NotEmpty
    @Size(max = 100)
    @Column(name = "inv_number")
    private String invNumber;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "inv_date")
    private LocalDate invDate;

    @Min(0)
    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "total_discount_amount")
    private BigDecimal totalDiscountAmount;

    @Column(name = "total_amount")
    private BigDecimal totalAmount;

    @OneToOne
    @JoinColumns(value = {
            @JoinColumn(name = "customer_id", referencedColumnName = "customer_id", insertable = false, updatable = false, nullable = false) })
    private Customer customer;

    @OneToMany(mappedBy = "sales", orphanRemoval = true)
    @JsonIgnoreProperties({ "sales" })
    private List<SalesInvoiceLine> line;

}
