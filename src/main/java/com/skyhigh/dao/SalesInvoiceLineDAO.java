package com.skyhigh.dao;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skyhigh.entity.SalesInvoice;
import com.skyhigh.entity.SalesInvoiceLine;
import com.skyhigh.serializable.SalesInvoiceLineSerializable;
import com.skyhigh.serializable.SalesInvoiceSerializable;

@Repository
public interface SalesInvoiceLineDAO extends JpaRepository<SalesInvoiceLine, SalesInvoiceLineSerializable> {

}
