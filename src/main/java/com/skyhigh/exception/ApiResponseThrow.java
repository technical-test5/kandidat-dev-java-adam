package com.skyhigh.exception;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class ApiResponseThrow extends Exception {
    private HttpStatus httpCode;
    private String status;
    private String message;
    private List<String> details;

    public ApiResponseThrow(HttpStatus http, String status, String message, List<String> details) {
        super();
        this.httpCode = http;
        this.status = status;
        this.message = message;
        this.details = details;
    }

    public ApiResponseThrow(HttpStatus http, String status, String message, String detail) {
        super();
        this.httpCode = http;
        this.status = status;
        this.message = message;
        details = Arrays.asList(detail);
    }
}
