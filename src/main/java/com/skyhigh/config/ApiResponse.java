package com.skyhigh.config;

import java.util.*;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ApiResponse {

    private HttpStatus httpCode;
    private String status;
    private String message;
    private List<String> details;

    public ApiResponse(HttpStatus http, String status, String message, List<String> details) {
        super();
        this.httpCode = http;
        this.status = status;
        this.message = message;
        this.details = details;
    }

    public ApiResponse(HttpStatus http, String status, String message, String detail) {
        super();
        this.httpCode = http;
        this.status = status;
        this.message = message;
        details = Arrays.asList(detail);
    }

}
