package com.skyhigh.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.fasterxml.jackson.databind.JsonMappingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.parsing.Problem;
import org.springframework.boot.json.JsonParseException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONException;

@ControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    // @Autowired
    // private HttpServletRequest requestServlet;
    // @ExceptionHandler(InvalidOrderItemException.class)
    // public void invalidOrderItem(HttpServletResponse response) throws IOException
    // {
    // response.setStatus(99);
    // }

    // Keterangan
    // Default handle, catch-all type of logic that deals with all other exceptions
    // that don't have specific handlers
    @ExceptionHandler({ Exception.class })
    protected ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        ApiResponse ApiResponse = new ApiResponse(
                HttpStatus.CONFLICT, "99", ex.getLocalizedMessage(), "Internal Server Error!");
        // ApiResponse.setDescription(requestServlet.getAttribute("description").toString());
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    @ExceptionHandler({ com.skyhigh.exception.ApiResponseThrow.class })
    protected ResponseEntity<Object> ApiResponseThrow(com.skyhigh.exception.ApiResponseThrow ex) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        ApiResponse ApiResponse = new ApiResponse(
                ex.getHttpCode(), ex.getStatus(), ex.getMessage(), ex.getDetails());
        // ApiResponse.setDescription(requestServlet.getAttribute("description").toString());
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    @ExceptionHandler(HttpMessageConversionException.class)
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageConversionException ex,
            HttpServletRequest request) {
        String message = "Parameter type error";
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        ApiResponse ApiResponse = new ApiResponse(
                HttpStatus.CONFLICT, "99", ex.getLocalizedMessage(), message);
        // ApiResponse.setDescription(requestServlet.getAttribute("description").toString());
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        ApiResponse ApiResponse = new ApiResponse(
                HttpStatus.CONFLICT, "99", ex.getLocalizedMessage(), "Internal Server Error!");
        // ApiResponse.setDescription(requestServlet.getAttribute("description").toString());
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    protected ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        ApiResponse ApiResponse = new ApiResponse(
                HttpStatus.OK, "99", ex.getLocalizedMessage(), "Internal Server Error!");
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    // Keterangan
    // BindException: This exception is thrown when fatal binding errors occur.
    // MethodArgumentNotValidException: This exception is thrown when argument
    // annotated with @Valid failed validation
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        List<String> errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        ApiResponse ApiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "99", ex.getLocalizedMessage(), errors);
        return handleExceptionInternal(
                ex, ApiResponse, headers, ApiResponse.getHttpCode(), request);

    }

    // Keterangan
    // MissingServletRequestPartException: This exception is thrown when when the
    // part of a multipart request not found
    // MissingServletRequestParameterException: This exception is thrown when
    // request missing parameter
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        String error = ex.getParameterName() + " parameter is missing";

        ApiResponse ApiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "99", ex.getLocalizedMessage(), error);
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    // Keterangan
    // ConstrainViolationException: This exception reports the result of constraint
    // violations
    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        List<String> errors = new ArrayList<String>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " +
                    violation.getPropertyPath() + ": " + violation.getMessage());
        }

        ApiResponse ApiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "99", ex.getLocalizedMessage(), errors);
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    // Keterangan
    // TypeMismatchException: This exception is thrown when try to set bean property
    // with wrong type.
    // MethodArgumentTypeMismatchException: This exception is thrown when method
    // argument is not the expected type
    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex, WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();

        ApiResponse ApiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "99", ex.getLocalizedMessage(), error);
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    // Keterangan
    // handle NoHandlerFoundException
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

        ApiResponse ApiResponse = new ApiResponse(HttpStatus.NOT_FOUND, "99", ex.getLocalizedMessage(), error);
        return new ResponseEntity<Object>(ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    // Keterangan
    // HttpRequestMethodNotSupportedException – which occurs when you send a
    // requested with an unsupported HTTP method
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(
                " method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));

        ApiResponse ApiResponse = new ApiResponse(HttpStatus.METHOD_NOT_ALLOWED, "99",
                ex.getLocalizedMessage(), builder.toString());
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

    // Keterangan
    // HttpMediaTypeNotSupportedException – which occurs when the client send a
    // request with unsupported media type
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        log.info("Error Handle Message ### :{}", ex.getMessage());
        log.info("Error Handle Cause ### :{}", ex.getCause());
        log.info("Error Handle Localized Message ### :{}", ex.getLocalizedMessage());
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t + ", "));

        ApiResponse ApiResponse = new ApiResponse(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "99",
                ex.getLocalizedMessage(), builder.substring(0, builder.length() - 2));
        return new ResponseEntity<Object>(
                ApiResponse, new HttpHeaders(), ApiResponse.getHttpCode());
    }

}
