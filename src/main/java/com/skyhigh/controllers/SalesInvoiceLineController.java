package com.skyhigh.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.commons.beanutils.BeanUtils;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
// import io.swagger.annotations.ApiResponse;
// import io.swagger.annotations.ApiResponses;
// import io.swagger.annotations.Example;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.skyhigh.dao.ProductDAO;
import com.skyhigh.dao.SalesInvoiceDAO;
import com.skyhigh.dao.SalesInvoiceLineDAO;
import com.skyhigh.dto.SalesInvoiceDTO;
import com.skyhigh.entity.SalesInvoice;
import com.skyhigh.entity.SalesInvoiceLine;
import com.skyhigh.exception.ApiResponseThrow;
import com.skyhigh.serializable.SalesInvoiceLineSerializable;
import com.skyhigh.service.SalesInvoiceService;

import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;

@Tag(description = "Untuk kebutuhan menu sales invoice line", name = "Menu Sales Invoice line")
@RestController
@RequestMapping("/api/v1/sales-invoice-line")
@Slf4j
public class SalesInvoiceLineController {

    @Autowired
    private SalesInvoiceLineDAO dao;
    @Autowired
    private ModelMapper ModelMapper;

    private Direction getSortDirection(String string) {
        // TODO Auto-generated method stub
        Direction result;
        if (string.equals("asc")) {
            result = Direction.ASC;
        } else {
            result = Direction.DESC;
        }
        return result;
    }

    @GetMapping
    public ResponseEntity<Map<String, Object>> salesLine(@RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "mProductId,asc") String[] sort) {
        log.info("START Sales Invoice Line");
        List<Order> order = new ArrayList<Order>();
        if (sort[0].contains(",")) {
            for (String sortOrder : sort) {
                String[] _sort = sortOrder.split(",");
                order.add(new Order(getSortDirection(_sort[1]), _sort[0]));
            }
        } else {
            order.add(new Order(getSortDirection(sort[1]), sort[0]));
        }
        if (size == 0) {
            size = Integer.MAX_VALUE;
        }
        Pageable pageable = PageRequest.of(page, size, Sort.by(order));
        // List<SalesInvoice> data = invoiceDAO.findAll();
        Page<SalesInvoiceLine> pagedResult = dao.findAll(pageable);
        Map<String, Object> result = new HashMap<>();
        result.put("httpCode", HttpStatus.OK);
        result.put("status", "00");
        result.put("message", "Success Get Sales invoice Line");
        result.put("details", pagedResult);
        log.info("END Sales Invoice Line");
        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }

    @Operation(summary = "Get detail Sales invoice", description = "Kegunaan untuk melihat detail sales invoice")
    @GetMapping("/{salesInvoice}/{salesInvoiceLine}")
    public ResponseEntity<Map<String, Object>> detailSalesInvoice(
            @PathVariable(name = "salesInvoice") UUID salesInvoice,
            @PathVariable(name = "salesInvoiceLine") UUID salesInvoiceLine)
            throws ApiResponseThrow {
        log.info("START Sales Invoice Line");
        SalesInvoiceLine pagedResult = dao
                .findById(new SalesInvoiceLineSerializable(
                        salesInvoiceLine,
                        salesInvoice))
                .orElseThrow(() -> new ApiResponseThrow(
                        HttpStatus.BAD_REQUEST, "99", "Sales Invoice Line with code = " + salesInvoiceLine
                                + " and Sales Invoice with code = " + salesInvoice + "not found",
                        ""));

        Map<String, Object> result = new HashMap<>();
        result.put("httpCode", HttpStatus.OK);
        result.put("status", "00");
        result.put("message", "Success Get Detail Sales invoice Line");
        result.put("details", pagedResult);
        log.info("END Sales Invoice Line");
        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }

    @Operation(summary = "save Sales invoice Line Batch", description = "Kegunaan untuk save sales invoice batch")
    @PostMapping
    public ResponseEntity<Map<String, Object>> saveSalesInvoiceLine(
            @Valid @RequestBody List<SalesInvoiceLine> request) throws ParseException, ApiResponseThrow {
        Map<String, Object> result = new HashMap<>();
        // try {
        log.info("START saveSalesInvoiceLine");
        log.info("DTO == {}", request);

        log.info("START Order CREATE");
        SalesInvoice resultMap = ModelMapper.map(request, SalesInvoice.class);
        log.info("ENTITY ORDER == {}", resultMap);
        dao.saveAll(request);
        log.info("END Order CREATE");

        result.put("httpCode", HttpStatus.OK);
        result.put("status", "00");
        result.put("message", "Success Save Invoice Line Batch");
        result.put("details", "Success");
        log.info("END saveSalesInvoiceLine");
        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }

    @Operation(summary = "update Sales invoice line", description = "Kegunaan untuk update sales invoice line")
    @PutMapping
    public ResponseEntity<Map<String, Object>> updateSalesInvoice(
            @Valid @RequestBody List<SalesInvoiceLine> request) throws ParseException, ApiResponseThrow {
        Map<String, Object> result = new HashMap<>();
        // try {
        log.info("START Update Sales Invoice");
        log.info("DTO == {}", request);

        log.info("START Order UPDATE");
        SalesInvoice resultMap = ModelMapper.map(request, SalesInvoice.class);
        log.info("ENTITY ORDER == {}", resultMap);
        dao.saveAll(request);
        log.info("END Order UPDATE");

        result.put("httpCode", HttpStatus.OK);
        result.put("status", "00");
        result.put("message", "Success Update Sales Invoice Line");
        result.put("details", "Success");
        log.info("END Update Sales Invoice Line");
        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }

    @Operation(summary = "delete Sales invoice Line", description = "Kegunaan untuk delete sales invoice Line")
    @DeleteMapping("/{salesInvoice}/{salesInvoiceLine}")
    public ResponseEntity<Map<String, Object>> deleteSalesInvoice(

            @PathVariable(name = "salesInvoice") UUID salesInvoice,
            @PathVariable(name = "salesInvoiceLine") UUID salesInvoiceLine) throws ParseException, ApiResponseThrow {
        Map<String, Object> result = new HashMap<>();
        // try {
        log.info("START Delete Sales Invoice Line");

        log.info("START Order DELETE");
        dao.deleteById(new SalesInvoiceLineSerializable(salesInvoiceLine, salesInvoice));
        log.info("END Order DELETE");

        result.put("httpCode", HttpStatus.OK);
        result.put("status", "00");
        result.put("message", "Success Delete Sales Invoice Line");
        result.put("details", "Success");
        log.info("END Delete Sales Invoice Line");
        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }
}
