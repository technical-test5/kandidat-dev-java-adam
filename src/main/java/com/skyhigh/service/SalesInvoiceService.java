package com.skyhigh.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import org.springframework.transaction.annotation.Transactional;

import com.skyhigh.dao.SalesInvoiceDAO;
import com.skyhigh.dao.SalesInvoiceLineDAO;
import com.skyhigh.entity.SalesInvoice;
import com.skyhigh.entity.SalesInvoiceLine;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class SalesInvoiceService {

    @Autowired
    private SalesInvoiceDAO invoiceDAO;

    @Autowired
    private SalesInvoiceLineDAO invoiceLineDAO;

    public void saveInvoice(SalesInvoice resultMap, List<SalesInvoiceLine> detail) {
        SalesInvoice header = invoiceDAO.save(resultMap);
        for (SalesInvoiceLine salesInvoiceLine : detail) {
            salesInvoiceLine.setTsalesInvoiceId(header.getSalesInvoiceId());
        }
        invoiceLineDAO.saveAll(detail);

    }

    public void deleteInvoice(UUID id) {
        invoiceDAO.deleteById(id);
    }

}
